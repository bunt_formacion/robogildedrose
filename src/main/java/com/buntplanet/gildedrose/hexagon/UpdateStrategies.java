package com.buntplanet.gildedrose.hexagon;

import java.util.HashMap;
import java.util.Map;

public abstract class UpdateStrategies {
  static Map<Class<? extends UpdateStrategy>, UpdateStrategy> strategies = new HashMap<Class<? extends UpdateStrategy>, UpdateStrategy>();

  static UpdateStrategy matching(Class<? extends UpdateStrategy> type) {
    return strategies.get(type);
  }

  public static void plug(UpdateStrategy... strategies) {
    for (UpdateStrategy strategy : strategies)
      UpdateStrategies.strategies.put(strategy.getClass(), strategy);
  }

  public static class Cheese extends UpdateStrategy {
    Cheese(EventsPort eventsPort) {
      super(eventsPort);
    }

    @Override
    void innerUpdate(Item item) {
      item.decreaseSellin();
      item.increaseQualityBy(1);
      if (item.hasExpired())
        item.increaseQualityBy(1);
    }
  }

  public static class Legendary extends UpdateStrategy {
    Legendary(EventsPort eventsPort) {
      super(eventsPort);
    }

    @Override
    void innerUpdate(Item item) {
    }
  }

  public static class Normal extends UpdateStrategy {
    Normal(EventsPort eventsPort) {
      super(eventsPort);
    }

    @Override
    void innerUpdate(Item item) {
      item.decreaseSellin();
      item.decreaseQualityBy(1);
      if (item.hasExpired())
        item.decreaseQualityBy(1);
    }
  }

  public static class Ticket extends UpdateStrategy {
    Ticket(EventsPort eventsPort) {
      super(eventsPort);
    }

    @Override
    void innerUpdate(Item item) {
      item.decreaseSellin();
      if (item.getSellIn() >= 10)
        item.increaseQualityBy(1);
      else if (item.getSellIn() >= 5)
        item.increaseQualityBy(2);
      else if (item.getSellIn() >= 0)
        item.increaseQualityBy(3);
      else
        item.setZeroQuality();
    }
  }

  public static class Conjured extends UpdateStrategy {
    Conjured(EventsPort eventsPort) {
      super(eventsPort);
    }

    @Override
    void innerUpdate(Item item) {
      item.decreaseSellin();
      item.decreaseQualityBy(2);
      if (item.hasExpired())
        item.decreaseQualityBy(2);
    }
  }
}
