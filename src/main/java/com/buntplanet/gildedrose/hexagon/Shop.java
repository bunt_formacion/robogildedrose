package com.buntplanet.gildedrose.hexagon;

class Shop {
  private final DataPort dataPort;

  Shop(DataPort dataPort) {
    this.dataPort = dataPort;
  }

  void update() {
    for (Item item : dataPort.getItems())
      item.update();
  }

}
