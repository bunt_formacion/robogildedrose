package com.buntplanet.gildedrose.hexagon;

import com.buntplanet.gildedrose.hexagon.domain.events.ZeroSellInReachedEvent;

public abstract class UpdateStrategy {
  private final EventsPort eventsPort;

  UpdateStrategy(EventsPort eventsPort) {
    this.eventsPort = eventsPort;
  }

  abstract void innerUpdate(Item item);

  void update(Item item) {
    innerUpdate(item);
    if (item.hasExpired())
      eventsPort.broadcast(new ZeroSellInReachedEvent(item));
  }
}
