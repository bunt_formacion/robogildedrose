package com.buntplanet.gildedrose.hexagon;

import com.buntplanet.gildedrose.hexagon.domain.events.UpdateEvent;

public class GildedRoseModule implements Runnable {
  private final Shop shop;
  private final EventsPort eventsPort;

  public GildedRoseModule(DataPort dataPort, EventsPort eventsPort) {
    this.shop = new Shop(dataPort);
    this.eventsPort = eventsPort;
  }

  @Override
  public void run() {
    UpdateStrategies.plug(new UpdateStrategies.Normal(eventsPort), new UpdateStrategies.Cheese(eventsPort), new UpdateStrategies.Conjured(eventsPort), new UpdateStrategies.Legendary(eventsPort), new UpdateStrategies.Ticket(eventsPort));
    eventsPort.on(UpdateEvent.class, new Callback<UpdateEvent>() {
      @Override
      public void call(UpdateEvent event) {
        shop.update();
      }
    });
  }
}
