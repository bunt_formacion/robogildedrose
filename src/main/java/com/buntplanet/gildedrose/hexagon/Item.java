package com.buntplanet.gildedrose.hexagon;

public final class Item {
  protected static final int MAX_QUALITY = 50;
  protected static final int MIN_QUALITY = 0;
  private final String name;
  private final UpdateStrategy updateStrategy;
  private int sellIn;
  private int quality;

  Item(String name, int sellIn, int quality, UpdateStrategy updateStrategy) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
    this.updateStrategy = updateStrategy;
  }

  public static Item factory(String name, int sellIn, int quality, Class<? extends UpdateStrategy> type) {
    return new Item(name, sellIn, quality, UpdateStrategies.matching(type));
  }

  void update() {
    updateStrategy.update(this);
  }

  void decreaseSellin() {
    sellIn--;
  }

  void decreaseQualityBy(int delta) {
    if (quality > MIN_QUALITY)
      quality -= delta;
  }

  void increaseQualityBy(int delta) {
    if (quality < MAX_QUALITY)
      quality += delta;
  }

  boolean hasExpired() {
    return sellIn < 0;
  }

  int getQuality() {
    return quality;
  }

  int getSellIn() {
    return sellIn;
  }

  void setZeroQuality() {
    quality = 0;
  }

  void expire() {
    sellIn = -1;
  }

  public String getName() {
    return name;
  }
}
