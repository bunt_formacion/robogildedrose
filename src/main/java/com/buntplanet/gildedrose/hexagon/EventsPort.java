package com.buntplanet.gildedrose.hexagon;

public interface EventsPort {
  <T> void on(Class<T> type, final Callback<T> command);

  void broadcast(Object event);
}
