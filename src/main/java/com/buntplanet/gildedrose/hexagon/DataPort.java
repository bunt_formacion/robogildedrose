package com.buntplanet.gildedrose.hexagon;

import java.util.List;

public interface DataPort {
  List<Item> getItems();

  void addItem(Item item);
}
