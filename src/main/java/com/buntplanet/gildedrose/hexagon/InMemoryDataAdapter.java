package com.buntplanet.gildedrose.hexagon;

import java.util.ArrayList;
import java.util.List;

public class InMemoryDataAdapter implements DataPort {
  List<Item> fixtures = new ArrayList<Item>();

  @Override
  public List<Item> getItems() {
    return fixtures;
  }

  @Override
  public void addItem(Item item) {
    fixtures.add(item);
  }
}
