package com.buntplanet.gildedrose.hexagon;

public interface Callback<T> {
  void call(T event);
}
