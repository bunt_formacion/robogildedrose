package com.buntplanet.gildedrose.hexagon.domain.events;

public interface Subscriber {
  <T extends Event> void onEvent(T event);
}
