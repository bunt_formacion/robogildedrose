package com.buntplanet.gildedrose.hexagon.domain.events;

import com.buntplanet.gildedrose.hexagon.Item;

public class ZeroSellInReachedEvent implements Event {
  private final Item item;

  public ZeroSellInReachedEvent(Item item) {
    this.item = item;
  }

  public Item getItem() {
    return item;
  }

  @Override
  public String toString() {
    return String.format("Zero SellIn Reached event for item %s", item.getName());
  }
}
