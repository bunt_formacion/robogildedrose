package com.buntplanet.gildedrose.hexagon.domain.events;

public class UpdateEvent implements Event {
  @Override
  public String toString() {
    return "Update event";
  }
}
