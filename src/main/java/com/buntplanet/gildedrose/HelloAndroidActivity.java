package com.buntplanet.gildedrose;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import com.buntplanet.gildedrose.hexagon.Callback;
import com.buntplanet.gildedrose.hexagon.GildedRoseModule;
import com.buntplanet.gildedrose.hexagon.InMemoryDataAdapter;
import com.buntplanet.gildedrose.hexagon.InMemoryEventsAdapter;
import com.buntplanet.gildedrose.hexagon.Item;
import com.buntplanet.gildedrose.hexagon.UpdateStrategies;
import com.buntplanet.gildedrose.hexagon.domain.events.UpdateEvent;
import com.buntplanet.gildedrose.hexagon.domain.events.ZeroSellInReachedEvent;

import java.util.ArrayList;
import java.util.List;

public class HelloAndroidActivity extends Activity implements View.OnClickListener {

  private Callback callback;
  private Button button;
  private ListView list;
  private List<Item> itemlist = new ArrayList<Item>();
  public InMemoryEventsAdapter eventsPort;
//  public OttoEventsAdapter eventsPort;

  @Override
  @SuppressWarnings("unchecked")
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    callback = new Callback<ZeroSellInReachedEvent>() {
      @Override
      public void call(ZeroSellInReachedEvent event) {
        if (!itemlist.contains(event.getItem())) {
          itemlist.add(event.getItem());
          ((ArrayAdapter<Item>) list.getAdapter()).notifyDataSetChanged();
        }
      }
    };

    setContentView(R.layout.activity_main);

    button = (Button) findViewById(R.id.button);
    list = (ListView) findViewById(R.id.list);

    button.setOnClickListener(this);
    list.setAdapter(new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, itemlist));

//    eventsPort = new OttoEventsAdapter(new Bus());
    eventsPort = new InMemoryEventsAdapter();
    InMemoryDataAdapter dataPort = new InMemoryDataAdapter();
    new GildedRoseModule(dataPort, eventsPort).run();
    dataPort.addItem(Item.factory("Cocotero", 1, 10, UpdateStrategies.Normal.class));
    dataPort.addItem(Item.factory("Chuchu", 2, 10, UpdateStrategies.Normal.class));
    dataPort.addItem(Item.factory("Blabla", 3, 10, UpdateStrategies.Normal.class));
  }

  @Override
  protected void onResume() {
    super.onResume();
    eventsPort.on(ZeroSellInReachedEvent.class, callback);
  }

  @Override
  protected void onPause() {
    super.onPause();
    //bus.unregister(ZeroSellInReachedEvent.class, callback);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(com.buntplanet.gildedrose.R.menu.main, menu);
    return true;
  }

  @Override
  public void onClick(View v) {
    eventsPort.broadcast(new UpdateEvent());
  }
}

