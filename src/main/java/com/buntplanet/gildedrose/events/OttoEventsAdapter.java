package com.buntplanet.gildedrose.events;

import com.buntplanet.gildedrose.hexagon.Callback;
import com.buntplanet.gildedrose.hexagon.EventsPort;
import com.squareup.otto.Bus;

import java.lang.reflect.InvocationTargetException;

public class OttoEventsAdapter implements EventsPort {
  private final Bus bus;

  public OttoEventsAdapter(Bus bus) {
    this.bus = bus;
  }

  @Override
  public <T> void on(Class<T> type, final Callback<T> callback) {
    bus.register(type, new com.squareup.otto.Callback<T>() {
      @Override
      public void call(T event) throws InvocationTargetException {
        callback.call(event);
      }
    });
  }

  @Override
  public void broadcast(Object event) {
    bus.post(event);
  }
}
