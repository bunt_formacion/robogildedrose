package com.buntplanet.gildedrose.hexagon;

import com.buntplanet.gildedrose.hexagon.domain.events.UpdateEvent;
import com.buntplanet.gildedrose.hexagon.domain.events.ZeroSellInReachedEvent;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class GildedRoseModuleTest {
  private UpdateStrategy zeroistStrategy;
  private UpdateStrategy expirationStrategy;
  private InMemoryEventsAdapter eventsPort;
  private InMemoryDataAdapter dataPort;

  @Before
  public void setUp() {

    dataPort = new InMemoryDataAdapter();
    eventsPort = new InMemoryEventsAdapter();
    zeroistStrategy = new UpdateStrategy(eventsPort) {
      @Override
      public void innerUpdate(Item item) {
        item.decreaseSellin();
        item.setZeroQuality();
      }
    };
    expirationStrategy = new UpdateStrategy(eventsPort) {
      @Override
      public void innerUpdate(Item item) {
        item.expire();
      }
    };
    UpdateStrategies.plug(expirationStrategy, zeroistStrategy);
    new GildedRoseModule(dataPort, eventsPort).run();
  }

  @Test
  public void normal_item_update_strategy_test() {
    Item item = Item.factory("+5 Dexterity Vest", 1, 10, UpdateStrategies.Normal.class);
    dataPort.addItem(item);

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(9, item.getQuality());
    assertEquals(0, item.getSellIn());

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(7, item.getQuality());
    assertEquals(-1, item.getSellIn());
  }

  @Test
  public void cheese_item_update_strategy_test() {
    Item item = Item.factory("Aged Brie", 1, 0, UpdateStrategies.Cheese.class);
    dataPort.addItem(item);

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(1, item.getQuality());
    assertEquals(0, item.getSellIn());

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(3, item.getQuality());
    assertEquals(-1, item.getSellIn());
  }

  @Test
  public void legendary_item_update_strategy_test() {
    Item item = Item.factory("Sulfuras", 0, 80, UpdateStrategies.Legendary.class);
    dataPort.addItem(item);

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(80, item.getQuality());
    assertEquals(0, item.getSellIn());
  }

  @Test
  public void ticket_item_update_strategy_test() {
    Item item = Item.factory("Concert Ticket", 20, 10, UpdateStrategies.Ticket.class);
    dataPort.addItem(item);

    manyUpdates(10);

    assertEquals(20, item.getQuality());
    assertEquals(10, item.getSellIn());

    manyUpdates(5);

    assertEquals(30, item.getQuality());
    assertEquals(5, item.getSellIn());
    manyUpdates(5);

    assertEquals(45, item.getQuality());
    assertEquals(0, item.getSellIn());

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(0, item.getQuality());
    assertEquals(-1, item.getSellIn());
  }

  @Test
  public void conjured_item_update_strategy_test() {
    Item item = Item.factory("Conjured Mana Cake", 1, 10, UpdateStrategies.Conjured.class);
    dataPort.addItem(item);

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(8, item.getQuality());
    assertEquals(0, item.getSellIn());

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(4, item.getQuality());
    assertEquals(-1, item.getSellIn());
  }

  @Test
  public void zeroist_item_update_strategy_test() {
    Item item = Item.factory("Humo", 10, 10, zeroistStrategy.getClass());
    dataPort.addItem(item);

    eventsPort.broadcast(new UpdateEvent());

    assertEquals(0, item.getQuality());
    assertEquals(9, item.getSellIn());
  }

  @Test
  public void cuando_un_producto_alcanza_sellIn_zero_el_modulo_nos_avisa() throws Exception {
    Item item = Item.factory("Humo", 10, 10, expirationStrategy.getClass());
    dataPort.addItem(item);

    eventsPort.broadcast(new UpdateEvent());

    Item actualItem = eventsPort.eventsOf(ZeroSellInReachedEvent.class).get(0).getItem();
    assertTrue(eventsPort.hasBeenFired(ZeroSellInReachedEvent.class));
    assertTrue(actualItem.hasExpired());
  }

  private void manyUpdates(int days) {
    for (int i = 0; i < days; i++)
      eventsPort.broadcast(new UpdateEvent());
  }

}
