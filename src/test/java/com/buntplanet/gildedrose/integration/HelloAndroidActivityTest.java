package com.buntplanet.gildedrose.integration;

import android.widget.Button;
import android.widget.ListView;
import com.buntplanet.gildedrose.HelloAndroidActivity;
import com.buntplanet.gildedrose.R;
import com.buntplanet.gildedrose.hexagon.Item;
import com.buntplanet.gildedrose.hexagon.UpdateStrategies;
import com.buntplanet.gildedrose.hexagon.domain.events.UpdateEvent;
import com.buntplanet.gildedrose.hexagon.domain.events.ZeroSellInReachedEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(RobolectricTestRunner.class)
public class HelloAndroidActivityTest {

  private HelloAndroidActivity activity;

  @Before
  public void setup(){
    activity = Robolectric.buildActivity(HelloAndroidActivity.class).create().resume().get();
  }

  @Test
  public void al_pulsar_el_boton_lanza_un_UpdateEvent() throws Exception {
    Button boton = (Button) activity.findViewById(R.id.button);
    boton.performClick();
    assertTrue(activity.eventsPort.hasBeenFired(UpdateEvent.class));
  }

  @Test
  public void cuando_el_hexagono_produce_un_ZeroSellInReachedEvent_mete_el_item_en_la_lista() throws Exception {
    Item item = Item.factory("COCOTERO", 10, 1, UpdateStrategies.Normal.class);
    activity.eventsPort.broadcast(new ZeroSellInReachedEvent(item));
    ListView list = (ListView) activity.findViewById(R.id.list);
    assertFalse(list.getAdapter().isEmpty());
    assertEquals(item, list.getAdapter().getItem(0));
  }
}
